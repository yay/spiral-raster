var app = angular.module('app', []);

app.directive('dnd', function () {
    return {
        link: function ($scope, elem, attr) {
            $(elem).addClass('dropbox');
            elem.on('dragover', function (e) {
                e.stopPropagation();
                e.preventDefault();
                // jQuery wraps native event object,
                // but we can still access it via 'originalEvent' property
                e.originalEvent.dataTransfer.dropEffect = 'copy';
                $(this).addClass('over');
            });
            elem.on('dragleave', function (e) {
                $(this).removeClass('over');
            });
            elem.on('drop', function (e) {
                e.stopPropagation();
                e.preventDefault();
                $(this).removeClass('over');
                $scope.handleDrop(e);
            });
        }
    }
});

app.directive('sample', function () {
    return {
        link: function ($scope, elem, attr) {
            var isDragging = false;
            elem.on('mousedown', function (e) {
                e.preventDefault();
                isDragging = true;
                $scope.onSample(e);
            });
            elem.on('mousemove', function (e) {
                if (isDragging) {
                    $scope.onSample(e);
                }
            });
            elem.on('mouseup mouseout', function (e) {
                isDragging = false;
            });
        }
    }
});

function Spiral($scope) {

    var spiralCanvas = document.getElementById('spiralCanvas');
    var originCanvas = document.getElementById('originCanvas');
    var colorBox = document.getElementById('colorBox');
    var ctx = spiralCanvas.getContext('2d');
    var octx = originCanvas.getContext('2d');
    var count = 0;

    $scope.useColor = false;
    $scope.sampleRadius = 3;

    $scope.handleDrop = function (e) {
        var file = e.originalEvent.dataTransfer.files[0];
        var reader = new FileReader();
        reader.onload = onFileLoad;
        reader.readAsDataURL(file);
        function onFileLoad(e) {
            var image = document.createElement('img');
            image.onload = function () {
                clearImage(octx);
                fitImage(octx, image);
                drawSpiral(ctx, octx);
            }
            image.src = e.target.result;
        }
    }

    $scope.onSample = function (e) {
        var event = e.originalEvent;
        var x = event.offsetX;
        var y = event.offsetY;
        // for some reason the type of input[type=range] value is 'string'
        var avgColor = getAverageColor(octx, x, y, +$scope.sampleRadius);

        colorBox.style.background = 'rgb(' + avgColor.join(',') + ')';
    }

    $scope.redraw = function () {
        drawSpiral(ctx, octx);
    }

    function toRad(degree) {
        return degree * Math.PI / 180;
    }

    function getAverageColor(ctx, cx, cy, r) {
        r = r || 3;

        var cw = ctx.canvas.width;
        var ch = ctx.canvas.height;
        var left = cx - r < 0 ? 0 : cx - r;
        var right = cx + r > cw ? cw : cx + r;
        var top = cy - r < 0 ? 0 : cy - r;
        var bottom = cy + r > ch ? ch : cy + r;
        var data = ctx.getImageData(left, top, right - left, bottom - top).data;
        var rgb = [0,0,0];

        for (var i = 0, n = data.length / 4; i < n; i++) {
            var index = i * 4;
            rgb[0] += data[index];
            rgb[1] += data[index+1];
            rgb[2] += data[index+2];
        }

        rgb[0] = ~~(rgb[0] / n);
        rgb[1] = ~~(rgb[1] / n);
        rgb[2] = ~~(rgb[2] / n);

        return rgb;
    }

    function getColorIntensity(rgb) {
        return (rgb[0] + rgb[1] + rgb[2]) / 3 / 255;
    }

    function clearImage(ctx) {
        var cw = ctx.canvas.width;
        var ch = ctx.canvas.height;
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, cw, ch); // don't use clearRect since we need color information
    }

    // scaling image proportionately
    function fitImage(ctx, img) {
        var cw = ctx.canvas.width;
        var ch = ctx.canvas.height;
        var iw = img.width;
        var ih = img.height;
        var w, h;
        if (iw > ih) {
            w = cw;
            h = ih / (iw / cw);
        } else {
            h = ch;
            w = iw / (ih / ch);
        }
        ctx.drawImage(img, (cw - w) / 2, (ch - h) / 2, w, h);
    }

    /**
     * Draws archimedean spiral (r = a * theta).
     * @param ctx Context to draw on.
     * @param octx Context to get image data from.
     */
    function drawSpiral(ctx, octx) {
        var a = 1;
        var count = 0;
        var cw = ctx.canvas.width;
        var ch = ctx.canvas.height;
        var dx = ~~(cw / 2);
        var dy = ~~(ch / 2);
        var ox = 0;
        var oy = 0;
        var maxR = ctx.canvas.height / 2;

        clearImage(ctx);

        ctx.save();
        ctx.translate(dx, dy);

        do {
            var theta = toRad(count++ * 5);
            var r = a * theta;
            var x = r * Math.cos(theta);
            var y = r * Math.sin(theta);
            var avgColor = getAverageColor(octx, x + dx, y + dy, +$scope.sampleRadius);
            var intensity = getColorIntensity(avgColor);
            var index = (x + y * cw) * 4;
            ctx.lineWidth = (1 - intensity) * 4;
            if ($scope.useColor) {
                ctx.strokeStyle = 'rgb(' + avgColor.join(',') + ')';
            }
            ctx.beginPath();
            ctx.moveTo(ox, oy);
            ctx.lineTo(x, y);
            ox = x;
            oy = y;
            ctx.stroke();
            ctx.closePath();
        } while (r < maxR);

        ctx.restore();
    }
}